/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    $('.deleteUser').on('click', deleteUser);
});

function deleteUser(){
   var confirmation = confirm('Are you sure?');
   
   if(confirmation){
       $.ajax({
          type: 'DELETE',
          url: '/users/delete/'+$(this).data('id')
       }).done(function(response){
           window.location.replace('/');
       });
       
       window.location.replace('/');
   }else{
       return false;
   }
}

