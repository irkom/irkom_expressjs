# Express js#


I am trying to create a simple web application with use Express js.

## What is Express? ##
ExpressJS is a minimalistic, open source web framework for Node.js
- Used to build powerful web applications and APIs
- Most popular framework for Node.js
- Uses MVC concepts


## Wha you will learn ##
- Express Installation and Setup
- Middleware
- Routing
- Template Engines - EJS, Handlebars, Jade
- Forms and Input
- Models, ORM & Databases - MongoDB
- Express Generator

## Installing Express ##
Requirements
[x] Node.js
[x] NPM

Commands:
$ npm init

$ npm install express --save

## Dependencies ##
[x] express
[x] body-parser
[x] nodemon
[x] ejs
[x] express-validator

## Install MongoDB ##
$npm install mongojs --save

## Run mongodb locally ##
- go to your MongoDB install directory
- go to bin
- run $ mongod
- fix following the error message
- back to bin folder
- run $ mongo

## backup and restore MongoDB ##
$ mongodump --db dbname

$ mongorestore --db dbname